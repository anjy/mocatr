﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using mocatr.Models;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Mvc;

namespace mocatr.Controllers
{
    public class HomeController : Controller
    {
        public HomeController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;

        }

        private ApplicationDbContext dbContext;
        private readonly UserManager<ApplicationUser> _userManager;


        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Profile(int id)
        {

            if (id < 0)
            {
                return HttpNotFound();
            }
            using (var context = new ApplicationDbContext())
            {

                //var model = context.Users.FirstOrDefault(r => r.UserName == "anjola@outlook.com");

                var model = _userManager.Users.FirstOrDefault(r => r.userid == id);

                if (model == null)
                {
                    return HttpNotFound();
                }
                return View(model);
                }
            }
        }
    }


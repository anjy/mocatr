﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;

namespace mocatr.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        [Key]
        public int userid { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string WorkEmail { get; set; }

        public string BusinessName { get; set; }

        public string Website { get; set; }

        public string Telephone { get; set; }
        public string AltTelephone { get; set; }

        private ApplicationDbContext _context;

        public int Commit()
        {
            
            return _context.SaveChanges();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mocatr.ViewModels.Account
{
    public class ProfileViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string WorkEmail { get; set; }

        public string BusinessName { get; set; }

        public string Website { get; set; }

        public string Telephone { get; set; }
        public string AltTelephone { get; set; }

    }
}
